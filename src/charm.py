#!/usr/bin/env python3
# Copyright 2020 Tom Barber
# See LICENSE file for licensing details.

import logging
from typing import Dict, Iterable, List
from ops.charm import CharmBase
from ops.main import main
from ops.framework import StoredState
from charmhelpers.core import host, hookenv
import yaml
import ops.charm
import ops.main
import ops.model

logger = logging.getLogger(__name__)
REQUIRED_SETTINGS = ["image"]

#zookeeper = ops.lib.use("zookeeper", 1, "tom@spicule.co.uk")

class SolrCharm(CharmBase):
    _stored = StoredState()

    def __init__(self, *args):
        super().__init__(*args)
        self.framework.observe(self.on.config_changed, self._on_config_changed)
        self.framework.observe(self.on.upgrade_charm, self._on_config_changed)
        self.framework.observe(self.on.start, self._on_config_changed)

        #self.zk = zookeeper.ZookeeperClient(self, 'db')
        #self.framework.observe(self.db.on.database_relation_joined, self.on_database_relation_joined)

    #def _on_database_relation_joined(self, event: zookeeper.DatabaseRelationJoinedEvent):
    #    pass

    def _on_config_changed(self, _):
        goal_state = hookenv.goal_state()

        logger.info("Goal state <<EOM\n{}\nEOM".format(yaml.dump(goal_state)))

            # Only the leader can set_spec().
        spec = self.make_pod_spec()
        resources = self.make_pod_resources()

        msg = "Configuring pod"
        logger.info(msg)
        self.model.unit.status = ops.model.MaintenanceStatus(msg)
        self.model.pod.set_spec(spec, {"kubernetesResources": resources})

        msg = "Pod configured"
        logger.info(msg)
        self.model.unit.status = ops.model.ActiveStatus(msg)


    def _on_fortune_action(self, event):
        fail = event.params["fail"]
        if fail:
            event.fail(fail)
        else:
            event.set_results({"fortune": "A bug in the code is worth two in the documentation."})

    def make_pod_spec(self) -> Dict:
        config = self.model.config
        env_config = {}
        env_config["SOLR_JAVA_MEM"] = config["javaOpts"]
        env_config["SOLR_LOG_LEVEL"] = config["logLevel"]
        image_details = {
                "imagePath": config["image"],
        }

        ports =  [
                {"name": "solr", "containerPort": 8983, "protocol": "TCP"},
            ]
        spec = {
                "version": 3,
                "containers": [
                    {
                        "name": self.app.name,
                        "imageDetails": image_details,
                        "imagePullPolicy": "Always",
                        "ports": ports,
                        "envConfig": env_config,
                        #"volumeConfig": vol_config,
		     }
                ],
            }

        logger.info(f"Pod spec <<EOM\n{yaml.dump(spec)}\nEOM")
        return spec

    def make_pod_resources(self) -> Dict:
        secrets_data = {}

        services = [
            {
                "name": "solr-port",
                "spec": {
                    "type": "NodePort",
                    "clusterIP": "",
                    "ports": [{"name": "solr", "port": 8983, "protocol": "TCP"}],
                },
            },
        ]

        resources = {
            "secrets": [{"name": "charm-secrets", "type": "Opaque", "data": secrets_data}],
            "services": services,
            "pod":{
                 "securityContext":{
                 "fsGroup": 1001,
                 "runAsUser": 1001,
                }
           },
        }
        logger.info(f"Pod resources <<EOM\n{yaml.dump(resources)}\nEOM")

        return resources

if __name__ == "__main__":
    main(SolrCharm)
